package com.cidenet.app.repository;

import com.cidenet.app.domain.Users;
import com.cidenet.app.domain.enumeration.EmploymentCountry;
import com.cidenet.app.domain.enumeration.IdType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<Users, Long>, JpaSpecificationExecutor<Users> {

    /**
     * search user by identification number
     * @param identification
     * @Optional<Users>
     */
    Optional<Users> findByIdentificationNumber(String identification);

    /**
     * search user by email
     * @param email
     * @Optional<Users>
     */
    Optional<Users> findByEmail(String email);

    /**
     * search all user to count
     * @Long
     */
    @Query("SELECT COUNT(id) FROM Users")
    Long CountAllUsers();

    /**
     * search user by email, name, otherName, surname, secondSurname or identificationNumber contains
     * @param email
     * @param name
     * @param otherName
     * @param surname
     * @param secondSurname
     * @param identificationNumber
     * @List<Users>
     */
    List<Users> findByEmailContainsOrNameContainsOrOtherNameContainsOrSurnameContainsOrSecondSurnameContainsOrIdentificationNumberContains(String email,
                                                                                                                                           String name,
                                                                                                                                           String otherName,
                                                                                                                                           String surname,
                                                                                                                                           String secondSurname,
                                                                                                                                           String identificationNumber);

    /**
     * search user pagination by state
     * @param state
     * @param pageable
     * @Page<Users>
     */
    Page<Users> findByState(Boolean state, Pageable pageable);

    /**
     * search user pagination by employee country
     * @param country
     * @param pageable
     * @Page<Users>
     */
    Page<Users> findByEmploymentCountry(EmploymentCountry country, Pageable pageable);

    /**
     * search user pagination by id type
     * @param idType
     * @param pageable
     * @return
     */
    Page<Users> findByIdType(IdType idType, Pageable pageable);
}
