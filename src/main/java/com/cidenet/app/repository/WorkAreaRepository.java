package com.cidenet.app.repository;

import com.cidenet.app.domain.WorkArea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface WorkAreaRepository extends JpaRepository<WorkArea, Long>, JpaSpecificationExecutor<WorkArea> {
}
