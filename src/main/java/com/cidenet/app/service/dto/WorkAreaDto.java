package com.cidenet.app.service.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
public class WorkAreaDto implements Serializable {
    Long id;
    String description;
}
