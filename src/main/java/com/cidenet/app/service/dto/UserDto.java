package com.cidenet.app.service.dto;

import com.cidenet.app.domain.WorkArea;
import com.cidenet.app.domain.enumeration.EmploymentCountry;
import com.cidenet.app.domain.enumeration.IdType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;


@Getter
@Setter
public class UserDto implements Serializable {

    private Long id;

    private String name;

    private String otherName;

    private String surname;

    private String secondSurname;

    private String email;

    private EmploymentCountry employmentCountry;

    private IdType idType;

    private String identificationNumber;

    private boolean state;

    private LocalDateTime registrationDate;

    private LocalDate admissionDate;

    private LocalDateTime lastUpdate;


    private WorkArea workArea;
}
