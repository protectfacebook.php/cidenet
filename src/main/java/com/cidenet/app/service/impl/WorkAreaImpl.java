package com.cidenet.app.service.impl;


import com.cidenet.app.repository.WorkAreaRepository;
import com.cidenet.app.service.IWorkAreaService;
import com.cidenet.app.service.dto.WorkAreaDto;
import com.cidenet.app.service.mapper.WorkAreaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkAreaImpl implements IWorkAreaService {

    private final WorkAreaRepository workAreaRepository;
    public WorkAreaImpl(WorkAreaRepository workAreaRepository) {
        this.workAreaRepository = workAreaRepository;
    }

    /**
     * create work area
     * @param dto
     * @WorkAreaDto
     */
    @Override
    public WorkAreaDto create(WorkAreaDto dto) {
        return WorkAreaMapper.WorkAreaEntityToDto(workAreaRepository.save(WorkAreaMapper.DtoToWorkAreaEntity(dto)));
    }

    /**
     * delete work area
     * @param id
     */

    @Override
    public void delete(Long id) {
        workAreaRepository.deleteById(id);
    }

    /**
     * search all work area list
     * @List<WorkAreaDto>
     */
    @Override
    public List<WorkAreaDto> getAll() {
        return workAreaRepository.findAll()
                .stream().map(WorkAreaMapper::WorkAreaEntityToDto)
                .collect(Collectors.toList());
    }

    /**
     * search work area by id
     * @param id
     * @WorkAreaDto
     */
    @Override
    public WorkAreaDto findById(Long id) {
        return WorkAreaMapper.WorkAreaEntityToDto(workAreaRepository.findById(id).get());
    }

    /**
     * update work area
     * @param dto
     * @WorkAreaDto
     */
    @Override
    public WorkAreaDto update(WorkAreaDto dto) {
        return WorkAreaMapper.WorkAreaEntityToDto(workAreaRepository.save(WorkAreaMapper.DtoToWorkAreaEntity(dto)));
    }
}
