package com.cidenet.app.service.impl;


import com.cidenet.app.domain.Users;
import com.cidenet.app.domain.enumeration.EmploymentCountry;
import com.cidenet.app.domain.enumeration.IdType;
import com.cidenet.app.repository.UserRepository;
import com.cidenet.app.service.IUserService;
import com.cidenet.app.service.dto.UserDto;
import com.cidenet.app.service.exception.ObjectError;
import com.cidenet.app.service.exception.ObjectException;
import com.cidenet.app.service.mapper.UserMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class UserServiceImpl implements IUserService {

    private final  Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * create user
     * @param user
     * @UserDto
     */
    @Override
    public UserDto createUser(UserDto user)  {
        String userEmail;
        Optional<Users> userFound = userRepository.findByIdentificationNumber(user.getIdentificationNumber());
        if (userFound.isPresent()){
            throw new ObjectException("Duplicate identification number",
                    new ObjectError(HttpStatus.BAD_REQUEST, "Already exist an user with this identification number"));
        }
        userEmail = generateEmail(user, false);
        Users userToSave = UserMapper.DtoToUserEntity(user);
        userToSave.setEmail(userEmail);
        userToSave.setName(userToSave.getName().toUpperCase());
        if (userToSave.getOtherName() == null || userToSave.getOtherName() == ""){
            userToSave.setOtherName(null);
        }else {
            userToSave.setOtherName(userToSave.getOtherName().toUpperCase());
        }
        userToSave.setSurname(userToSave.getSurname().toUpperCase());
        userToSave.setSecondSurname(userToSave.getSecondSurname().toUpperCase());
        userToSave.setRegistrationDate(LocalDateTime.now());
        userToSave.setState(true);
        return UserMapper.UserEntityToDto(userRepository.save(userToSave));

    }

    /**
     * generate email to user
     * @param user
     * @param regeneration
     * @String
     */
    private String  generateEmail(UserDto user,boolean regeneration){
        StringBuilder userEmail = new StringBuilder();
        userEmail.append(user.getName()).append('.').append(user.getSurname().replace(" ", ""));
        switch (user.getEmploymentCountry()){
            case USA:
                userEmail.append("@cidenet.com.us");
                break;
            case CO:
                userEmail.append("@cidenet.com.co");
                break;
            default:
                userEmail.append("@cidenet.com.co");
                logger.debug("Default email domain: @cidenet.com.co");
                break;

        }
        if(!regeneration){
            Optional<Users> userFound = userRepository.findByEmail(userEmail.toString());
            if (userFound.isPresent()){
                userEmail.insert(userEmail.indexOf("@") , "." + userRepository.CountAllUsers() );
            }
        }else {
            userEmail.insert(userEmail.indexOf("@") , "." + userRepository.CountAllUsers() );
        }

        return userEmail.toString();
    }

    /**
     * delete user
     * @param id
     */
    @Override
    public void delete(Long id) {
        logger.debug("Request  to delete User : {}" , id);
        userRepository.deleteById(id);
    }

    /**
     * update user
     * @param userDto
     * @UserDto
     */
    @Override
    public UserDto update(UserDto userDto) {
        logger.debug("Request to update User: {}" , userDto);
        Users userToSave = UserMapper.DtoToUserEntity(userDto);
        Users userFound = userRepository.findByIdentificationNumber(userDto.getIdentificationNumber()).get();
        if (userFound != null){
            if (!userFound.getIdentificationNumber().equals(userDto.getIdentificationNumber())){
                throw new ObjectException("Duplicate identification number",
                        new ObjectError(HttpStatus.BAD_REQUEST, "Already exist an user with this identification number"));
            }else {
                userToSave.setRegistrationDate(userFound.getRegistrationDate());
            }
        }
        userToSave.setState(true);
        userToSave.setLastUpdate(LocalDateTime.now());
        userToSave.setName(userToSave.getName().toUpperCase());
        if (userToSave.getOtherName() == null || userToSave.getOtherName() == ""){
            userToSave.setOtherName(null);
        }else {
            userToSave.setOtherName(userToSave.getOtherName().toUpperCase());
        }
        userToSave.setSurname(userToSave.getSurname().toUpperCase());
        userToSave.setSecondSurname(userToSave.getSecondSurname().toUpperCase());
        userToSave.setEmail(generateEmail(userDto, true));
        return UserMapper.UserEntityToDto(userRepository.save(userToSave));
    }

    /**
     * search user pagination
     * @param pageNumber
     * @param pageSize
     * @Page<UserDto>
     */
    @Override
    public Page<UserDto> getPaginatedUsers(int pageNumber, int pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return userRepository.findAll(pageable)
                .map(UserMapper::UserEntityToDto);
    }

    /**
     * search user by id
     * @param id
     * @UserDto
     */
    @Override
    public UserDto findById(Long id) {
        return UserMapper.UserEntityToDto(userRepository.findById(id).get());
    }

    /**
     * search user pagination by criteria
     * @param criteria
     * @param page
     * @param size
     * @Page<UserDto>
     */
    @Override
    public Page<UserDto> getPaginatedUsersByCriteria(String criteria, int page, int size) {
        List<Users> usersFound = userRepository.
                findByEmailContainsOrNameContainsOrOtherNameContainsOrSurnameContainsOrSecondSurnameContainsOrIdentificationNumberContains(criteria,
                        criteria,criteria, criteria, criteria, criteria);
        if (usersFound.size() < 10){
            size = usersFound.size();
            page = 0;
        }
        Pageable pageable = PageRequest.of(page, size);
        Page<UserDto> usersPaginated = new PageImpl<UserDto>(usersFound.stream().map(UserMapper::UserEntityToDto).collect(Collectors.toList()),
                pageable, usersFound.size());
        return usersPaginated;
    }

    /**
     * search user pagination by state
     * @param state
     * @param page
     * @param size
     * @Page<UserDto>
     */
    @Override
    public Page<UserDto> getPaginatedUsersByState(Boolean state, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findByState(state, pageable).map(UserMapper::UserEntityToDto);
    }

    /**
     * search user pagination by country
     * @param country
     * @param page
     * @param size
     * @Page<UserDto>
     */
    @Override
    public Page<UserDto> getPaginatedUsersByEmploymentCountry(EmploymentCountry country, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findByEmploymentCountry(country, pageable).map(UserMapper::UserEntityToDto);
    }

    /**
     * search user pagination by document type
     * @param idType
     * @param page
     * @param size
     * @Page<UserDto>
     */
    @Override
    public Page<UserDto> getPaginatedUsersByDocumentType(IdType idType, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        return userRepository.findByIdType(idType, pageable).map(UserMapper::UserEntityToDto);
    }
}
