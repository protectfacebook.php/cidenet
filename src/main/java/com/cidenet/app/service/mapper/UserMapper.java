package com.cidenet.app.service.mapper;

import com.cidenet.app.domain.Users;
import com.cidenet.app.service.dto.UserDto;

public class UserMapper {


    public static UserDto UserEntityToDto(Users user){
        if (user  == null) return null;
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setName(user.getName());
        dto.setOtherName(user.getOtherName());
        dto.setSurname(user.getSurname());
        dto.setSecondSurname(user.getSecondSurname());
        dto.setEmail(user.getEmail());
        dto.setEmploymentCountry(user.getEmploymentCountry());
        dto.setIdType(user.getIdType());
        dto.setIdentificationNumber(user.getIdentificationNumber());
        dto.setState(dto.isState());
        dto.setRegistrationDate(user.getRegistrationDate());
        dto.setAdmissionDate(user.getAdmissionDate());
        dto.setLastUpdate(user.getLastUpdate());

        dto.setWorkArea(user.getWorkArea());
        return dto;
    }
    public static Users DtoToUserEntity(UserDto dto){
        if (dto == null) return null;
        Users users = new Users();
        users.setId(dto.getId());
        users.setName(dto.getName());
        users.setOtherName(dto.getOtherName());
        users.setSurname(dto.getSurname());
        users.setSecondSurname(dto.getSecondSurname());
        users.setEmail(dto.getEmail());
        users.setEmploymentCountry(dto.getEmploymentCountry());
        users.setIdType(dto.getIdType());
        users.setIdentificationNumber(dto.getIdentificationNumber());
        users.setState(dto.isState());
        users.setRegistrationDate(dto.getRegistrationDate());
        users.setAdmissionDate(dto.getAdmissionDate());
        users.setLastUpdate(dto.getLastUpdate());
        users.setLastUpdate(dto.getLastUpdate());

        users.setWorkArea(dto.getWorkArea());
        return users;
    }
}
