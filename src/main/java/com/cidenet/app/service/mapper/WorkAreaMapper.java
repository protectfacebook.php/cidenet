package com.cidenet.app.service.mapper;

import com.cidenet.app.domain.WorkArea;
import com.cidenet.app.service.dto.WorkAreaDto;

public class WorkAreaMapper {


    public static WorkAreaDto WorkAreaEntityToDto(WorkArea workArea){
        if (workArea == null) return null;
        WorkAreaDto dto = new WorkAreaDto();
        dto.setId(workArea.getId());
        dto.setDescription(workArea.getDescription());
        return dto;
    }

    public static WorkArea DtoToWorkAreaEntity(WorkAreaDto dto){
        if (dto == null) return null;
        WorkArea workArea = new WorkArea();
        workArea.setId(dto.getId());
        workArea.setDescription(dto.getDescription());
        return workArea;
    }
}
