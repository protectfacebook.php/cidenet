package com.cidenet.app.service;

import com.cidenet.app.domain.enumeration.EmploymentCountry;
import com.cidenet.app.domain.enumeration.IdType;
import com.cidenet.app.service.dto.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

/**
 * each of the methods that will be used in the REST CONTROLLER are declared in this interface
 */
public interface IUserService {
    /**
     * create user
     * @param user
     * @UserDto
     */
    UserDto createUser(UserDto user)  ;

    /**
     * delete user
     * @param id
     */
    void delete(Long id);

    /**
     * update user
     * @param user
     * @return
     */
    UserDto update(UserDto user);
    /**
     * search user pagination
     * @param pageNumber
     * @param pageSize
     * @Page<UserDto>
     */
    Page<UserDto> getPaginatedUsers(int pageNumber, int pageSize);
    /**
     * search user by id
     * @param id
     * @UserDto
     */
    UserDto findById(Long id);

    /**
     * search user pagination by criteria
     * @param criteria
     * @param page
     * @param size
     * @Page<UserDto>
     */
    Page<UserDto> getPaginatedUsersByCriteria(String criteria, int page, int size);
    /**
     * search user pagination by state
     * @param state
     * @param page
     * @param size
     * @Page<UserDto>
     */
    Page<UserDto> getPaginatedUsersByState(Boolean state, int page, int size);
    /**
     * search user pagination by country
     * @param country
     * @param page
     * @param size
     * @Page<UserDto>
     */
    Page<UserDto> getPaginatedUsersByEmploymentCountry(EmploymentCountry country, int page, int size);

    /**
     * search user pagination by document type
     * @param idType
     * @param page
     * @param size
     * @Page<UserDto>
     */
    Page<UserDto> getPaginatedUsersByDocumentType(IdType idType, int page, int size);
}
