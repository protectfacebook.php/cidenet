package com.cidenet.app.service;

import com.cidenet.app.service.dto.WorkAreaDto;

import java.util.List;

public interface IWorkAreaService {

    /**
     * create work area
     * @param dto
     * @WorkAreaDto
     */
    WorkAreaDto create(WorkAreaDto dto);
    /**
     * delete work area
     * @param id
     */
    void delete(Long id);

    /**
     * search all work area list
     * @List<WorkAreaDto>
     */
    List<WorkAreaDto> getAll();

    /**
     * search work area by id
     * @param id
     * @WorkAreaDto
     */
    WorkAreaDto findById(Long id);

    /**
     * update work area
     * @param dto
     * @WorkAreaDto
     */
    WorkAreaDto update(WorkAreaDto dto);
}
