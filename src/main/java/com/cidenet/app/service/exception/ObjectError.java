package com.cidenet.app.service.exception;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Setter
@Getter
@AllArgsConstructor
public class ObjectError {
    private final HttpStatus status;
    private final String description;
}
