package com.cidenet.app.service.exception;


import lombok.Getter;

@Getter
public class ObjectException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    private final transient ObjectError objectError;

    public ObjectException(String message, ObjectError objectError){
        super(message);
        this.objectError = objectError;
    }
}
