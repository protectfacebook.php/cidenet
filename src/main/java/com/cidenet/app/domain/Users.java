package com.cidenet.app.domain;


import com.cidenet.app.domain.enumeration.EmploymentCountry;
import com.cidenet.app.domain.enumeration.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, columnDefinition = "serial")
    private Long id;


    @Column(unique = false, nullable = false, length = 20)
    private String name;

    @Column(unique = false, length = 50)
    private String otherName;

    @Column(unique = false, nullable = false, length = 20)
    private String surname;

    @Column(unique = false, nullable = false, length = 20)
    private String secondSurname;

    @Column(unique = true, nullable = false, length = 300)
    private String email;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private EmploymentCountry employmentCountry;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private IdType idType;

    @Column(unique = true, nullable = false, length = 20)
    private String identificationNumber;

    @Column(nullable = false)
    private boolean state;

    @JsonFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss")
    private LocalDateTime registrationDate;

    @JsonFormat(pattern = "YYYY-MM-dd")
    private LocalDate admissionDate;

    @JsonFormat(pattern = "dd-MM-yyyy'T'HH:mm:ss")
    private LocalDateTime lastUpdate;


    @ManyToOne(optional = true)
    private WorkArea workArea;


}














