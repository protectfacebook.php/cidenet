package com.cidenet.app.domain.enumeration;

public enum IdType {
    CC, CE, PASSPORT, SP
}
