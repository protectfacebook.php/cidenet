package com.cidenet.app.web.rest;


import com.cidenet.app.service.IWorkAreaService;
import com.cidenet.app.service.dto.WorkAreaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class WorkAreaResource {

    @Autowired
    private IWorkAreaService iWorkAreaService;

    /**
     * this method save a work-area into db
     * @param dto this param is the work-area for creating
     * @return the work-area saved
     */
    @PostMapping("/work-area")
    public ResponseEntity<WorkAreaDto> create(@RequestBody WorkAreaDto dto){
        return new ResponseEntity<>(iWorkAreaService.create(dto), HttpStatus.OK);
    }


    /**
     * this method delete an element by id
     * @param id the param for delete de work-area
     */
    @DeleteMapping("/work-area/{id}")
    public void delete(@PathVariable(value = "id") Long id){
        iWorkAreaService.delete(id);
    }


    /**
     * this method obtains all work-area records
     * @return work-area list
     */
    @GetMapping("/work-area")
    public List<WorkAreaDto> getAll(){
        return iWorkAreaService.getAll();
    }


    /**
     * this method update an existing work-area
     * @param dto the work-area for updating
     * @return the work-area updates
     */
    @PutMapping("/work-area")
    public ResponseEntity<WorkAreaDto> update(@RequestBody WorkAreaDto dto){
        return  new ResponseEntity<>(iWorkAreaService.update(dto), HttpStatus.OK);
    }

    /**
     * this method obtains a work-area by id
     * @param id the attribute for searching the work-area
     * @return
     */
    @GetMapping("/work-area/{id}")
    public WorkAreaDto findById(@PathVariable(value = "id") Long id){
        return iWorkAreaService.findById(id);
    }
}
