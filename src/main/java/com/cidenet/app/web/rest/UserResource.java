package com.cidenet.app.web.rest;


import com.cidenet.app.domain.enumeration.EmploymentCountry;
import com.cidenet.app.domain.enumeration.IdType;
import com.cidenet.app.service.IUserService;
import com.cidenet.app.service.dto.UserDto;
import com.cidenet.app.service.exception.ObjectError;
import com.cidenet.app.service.exception.ObjectException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserResource {


    /**
     * here the service is being integrated
     */
    @Autowired
    private IUserService iUserService;


    /**
     *
     * @param dto
     * @return the user created
     */
    @PostMapping("/users")
    ResponseEntity<UserDto> createUser(@RequestBody UserDto dto)   {
        return new ResponseEntity<>(iUserService.createUser(dto), HttpStatus.OK);
    }


    /**
     * This method delete an user from database
     * @param id param for  deleting user by their id
     */
    @DeleteMapping("/users/{id}")
    void delete(@PathVariable(value = "id") Long  id){
        iUserService.delete(id);
    }

    /**
     * this method works with an user dto, this dto should have an id for updating
     * @param dto
     * @return the user updated
     */
    @PutMapping("/users")
    ResponseEntity<UserDto> update(@RequestBody UserDto dto)  {
        if (dto.getId() == null){
            throw new ObjectException("invalid id",
                    new ObjectError(HttpStatus.BAD_REQUEST, "the field ID couldn't be empty"));
        }
        return  new ResponseEntity<>(iUserService.update(dto), HttpStatus.OK);
    }


    /**
     * this method search an user by id
     * @param id
     * @return the user found
     */
    @GetMapping("/users/{id}")
    ResponseEntity<UserDto> findById(@PathVariable(value = "id") Long  id){
        return new ResponseEntity<>(iUserService.findById(id), HttpStatus.OK);
    }


    /**
     * this method return a Page Object with an user list
     * @param page this param is  the number of page to search
     * @param size this param define the page size
     * @return
     */
    @GetMapping("/users/paginated")
    Page<UserDto> getPaginatedUsers(@RequestParam(value = "page") int page, @RequestParam(value = "size") int size ){
        return iUserService.getPaginatedUsers(page, size);
    }


    /**
     * this method search in db and return a paginated user list
     * @param criteria the param for searching
     * @param page
     * @param size this param define the page size
     * @return
     */
    @GetMapping("/users/paginated-by-criteria")
    Page<UserDto> getPaginatedUsersByCriteria(@RequestParam(value = "criteria") String criteria,
                                              @RequestParam(value = "page") int page,
                                              @RequestParam(value = "size") int size){
        return iUserService.getPaginatedUsersByCriteria(criteria.toUpperCase(), page, size);
    }


    /**
     * this method search into db an user list, after search this paginate the list found.
     * @param state the state for searching
     * @param page the number page for return data
     * @param size the page size
     * @return a Page object with an user list
     */

    @GetMapping("/users/paginated-by-state")
    Page<UserDto> getPaginatedUserByState(@RequestParam(value = "state") Boolean state,
                                          @RequestParam(value = "page") int page,
                                          @RequestParam(value = "size") int size){
        return iUserService.getPaginatedUsersByState(state, page, size);
    }


    /**
     *this method search into db an user list, after search this paginate the list found.
     * @param country the employmentCountry for searching
     * @param page the number page for return data
     * @param size the page size
     * @return a Page object with an user list
     */
    @GetMapping("/users/paginated-by-employment-country")
    Page<UserDto> getPaginatedUsersByEmploymentCountry(@RequestParam(value = "country")EmploymentCountry country,
                                                       @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "size") int size){
        return  iUserService.getPaginatedUsersByEmploymentCountry(country, page, size);
    }

    /**
     * this method search into db an user list, after search this paginate the list found.
     * @param documentType the IdType for searching
     * @param page the number page for return data
     * @param size the number page for return data
     * @return a Page object with an user list
     */
    @GetMapping("/users/paginated-by-document-type")
    Page<UserDto> getPaginatedUsersByDocumentType(@RequestParam(value ="documentType")IdType documentType,
                                                  @RequestParam(value = "page") int page,
                                                  @RequestParam(value = "size") int size){
        return iUserService.getPaginatedUsersByDocumentType(documentType, page, size);
    }





}
